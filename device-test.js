require('dotenv').config()
var awsIot = require('aws-iot-device-sdk');

console.log(process.env.AWS_ENVIRONMENT)
//
// Replace the values of '<YourUniqueClientIdentifier>' and '<YourCustomEndpoint>'
// with a unique client identifier and custom host endpoint provided in AWS IoT.
// NOTE: client identifiers must be unique within your AWS account; if a client attempts 
// to connect with a client identifier which is already in use, the existing 
// connection will be terminated.
//
var device = awsIot.device({
   keyPath: '../main/certs/private.pem.key',
  certPath: '../main/certs/certificate.pem.crt',
    caPath: '../main/certs/aws-root-ca.pem',
  clientId: 'switch1',
      host: 'a2zopw3obz2cdd.iot.ap-southeast-1.amazonaws.com',
      port: 8883
});

//
// Device is an instance returned by mqtt.Client(), see mqtt.js for full
// documentation.
//
device
  .on('connect', function() {
    console.log('connect');
    device.subscribe('switch1');
    setInterval(function() {
        console.log('publish');
        device.publish('switch1', JSON.stringify({ test_data: 1}));
    }, 1000);
  });
device
    .on('close', function() {
        console.log('close');
    });
device
    .on('reconnect', function() {
        console.log('reconnect');
    });
device
    .on('offline', function() {
        console.log('offline');
    });
device
    .on('error', function(error) {
        console.log('error', error);
    });
device
    .on('publish', function(topic) {
        console.log('publish', topic);
    });
device
    .on('message', function(topic, payload) {
        console.log('message', topic, payload.toString());
    });
