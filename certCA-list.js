require('dotenv').config()

var AWS = require('aws-sdk');
// AWS SDK was loaded after bluebird, set promise dependency
AWS.config.setPromisesDependency(Promise);
var iot = new AWS.Iot();

iot.listCACertificates()
.promise()
.then(function(data){
    console.log(data)
})
.catch(function(err){
    console.log(err)
})