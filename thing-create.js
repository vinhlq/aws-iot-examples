crypto=require('crypto');
require('dotenv').config()

var AWS = require('aws-sdk');
// AWS SDK was loaded after bluebird, set promise dependency
AWS.config.setPromisesDependency(Promise);
var iot = new AWS.Iot();
var iotdata = new AWS.IotData({endpoint: "a2zopw3obz2cdd.iot.ap-southeast-1.amazonaws.com"});

var serialNumber = "SN-"+crypto.randomBytes(Math.ceil(12/2)).toString('hex').slice(0,15).toUpperCase();
var clientId = "ID-"+crypto.randomBytes(Math.ceil(12/2)).toString('hex').slice(0,12).toUpperCase();
var activationCode = "AC-"+crypto.randomBytes(Math.ceil(20/2)).toString('hex').slice(0,20).toUpperCase();

var thingParams = {
    thingName: "thing-" + clientId
};

iot.createThing(thingParams)
.promise()
.then(function(data){
    console.log(data);
})
.catch(function(err){
    console.error(err)
});