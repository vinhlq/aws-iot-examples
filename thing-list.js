require('dotenv').config()
var AWS = require('aws-sdk');
// AWS SDK was loaded after bluebird, set promise dependency
AWS.config.setPromisesDependency(Promise);
var iot = new AWS.Iot();

iot.describeEndpoint()
.promise()
.then(function(data){
    console.log("endpoint: " + JSON.stringify(data));
    
    iot.listThings()
    .promise()
    .then(function(data){
        console.log("things: " + JSON.stringify(data.things));
    })
    .catch(function(err){
        console.error(err);
    });
})
.catch(function(err){
    console.error(err);
});